import { Component, OnInit } from '@angular/core';
import { GetInfoService } from '../../services/get-info.service';
import { SaveInfoService } from '../../models/save-info.service';



@Component({
    selector: 'app-ds',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent  implements OnInit {

    infoData : object;
    sidebarFlag : boolean;

    constructor(private getInfoService: GetInfoService, private saveInfoService: SaveInfoService) {}

  	ngOnInit() {
        this.infoData = this.saveInfoService;
        this.sidebarFlag = true;
  	}

  	getInfo() {
	  	this.getInfoService.getInfo()
	  	.then((response) => {
			response.forEach(element => {
			    console.log(element['Name']);
			});
	  	})
	 }

    displaySidebar() {
        this.sidebarFlag = !this.sidebarFlag;
    }

}
