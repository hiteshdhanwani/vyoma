import { Component, OnInit } from '@angular/core';
import { GetInfoService } from '../../services/get-info.service';
import { SaveInfoService } from '../../models/save-info.service';

@Component({
  	selector: 'app-selection',
  	templateUrl: './selection.component.html',
  	styleUrls: ['./selection.component.css']
})
export class SelectionComponent implements OnInit{

	infoToDisplay : object;
	info = [];

  	constructor(private getInfoService: GetInfoService, private saveInfoService: SaveInfoService) {}

  	ngOnInit() {
  		this.getInfo();
  		this.filterData();
  	}
  		
	cities = ['Bangalore','BMTC','Delhi','Hyderabad'];
	selectedCities = ['Bangalore'];

  	selectCities(citi) {
	    var citiId = this.selectedCities.indexOf(citi);

	    if (citiId > -1) {
	      	this.selectedCities.splice(citiId, 1);
	      	this.filterData();
	    }
	    else {
	      	this.selectedCities.push(citi);
	      	this.filterData();
	    }
  	};

  	filterData() {
  		this.infoToDisplay = [];
  		var activeHosts = 0;
  		var totalHost = 0;
  		var adsNotDisplayed = 0;
  		var adsUserNotDisplayed = 0;
  		var name = 0;
  		var systemUnavailable = 0;
  		var userInfoNotDisplayed = 0;
  		var workingScreen = 0;
		this.info.forEach((value) => {
			if(this.selectedCities.length>0) {
				var citiStatus = this.selectedCities.includes(value['Name']);
			}
			else {
				var citiStatus = this.cities.includes(value['Name']);
			}
			if (citiStatus == true) {
				activeHosts = activeHosts + parseInt(value['Active_Hosts']);
				totalHost = totalHost + parseInt(value['Total_Host']);
				adsNotDisplayed = adsNotDisplayed + parseInt(value['Ads_Not_Displayed']);
				adsUserNotDisplayed = adsUserNotDisplayed + parseInt(value['Ads_User_Not_Displayed']);
				systemUnavailable = systemUnavailable + parseInt(value['System_Unavailable']);
				userInfoNotDisplayed = userInfoNotDisplayed + parseInt(value['UserInfo_Not_Displayed']);
				workingScreen = workingScreen + parseInt(value['Working_Screens']);
			}
		})
		this.infoToDisplay = {
			'activeHosts' : activeHosts,
			'totalHost' : totalHost,
			'adsNotDisplayed' : adsNotDisplayed,
			'adsUserNotDisplayed' : adsUserNotDisplayed,
			'systemUnavailable' : systemUnavailable,
			'userInfoNotDisplayed' : userInfoNotDisplayed,
			'workingScreen' : workingScreen
		}
		this.saveInfoService.setInfo(this.infoToDisplay);
  	};

  	clearAll() {
  		this.selectedCities = [];
  		this.filterData();
  	}

  	getInfo() {
	  	this.getInfoService.getInfo()
	  	.then((response) => {
			this.info = response;
	      	this.filterData();
	  	})
	}
}
