import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

/* impport components*/

import { AppComponent } from './app.component';
import { SelectionComponent } from './components/selection/selection.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';


/* import services*/

import { GetInfoService } from './services/get-info.service';
import { SaveInfoService } from './models/save-info.service';

const appRoutes: Routes = [
    {
        path: 'dashboard',
        component : DashboardComponent
    }, 
    { 
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    }
]

@NgModule({
  declarations: [
    AppComponent,
    SelectionComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpClientModule,
    Ng4LoadingSpinnerModule.forRoot(),
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot(appRoutes)

  ],
  providers: [
        GetInfoService,
        SaveInfoService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
