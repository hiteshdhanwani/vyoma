import { Injectable } from '@angular/core';

@Injectable()
export class SaveInfoService {
	
	info = null;

  	constructor() { }

  	setInfo(data) {
  		this.info = data;
  	}

  	getInfo() {
  		return this.info;
  	}

}
