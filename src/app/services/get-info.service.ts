import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class GetInfoService {

	constructor(private http: HttpClient) { }

	getInfo(): Promise<any> {
		let url:string = 'assets/data/values.json';
		return this.http.get(url).toPromise();
	}

}
